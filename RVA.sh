#!/bin/bash
clear

echo -e "\e[92m                 ,  , "
echo -e "\e[92m                / \/ \,'| _ "
echo -e "\e[92m               ,'    '  ,' |,| "
echo -e "\e[92m              ,'           ' |,'| "
echo -e "\e[92m             ,'                 ;'| _ "
echo -e "\e[92m            ,'                    '' | "
echo -e "\e[92m           ,'                        ;-, "
echo -e "\e[92m          (___                        / "
echo -e "\e[92m        ,'    '.  ___               ,' "
echo -e "\e[92m       :       ,''   '-.           / "
echo -e "\e[92m       |-._ o /         \         / "
echo -e "\e[92m      (    '-(           )       / "
echo -e "\e[92m     ,''.     \      o  /      ,' "
echo -e "\e[92m    /    `     `.     ,'      /   \t\t\t\e[101mOlá $USER!\e[0m"
echo -e "\e[92m   (             \`\"\"\"'       /    "
echo -e "\e[92m    \`._                     /  \t\e[33mVai proceder à instalação de software"
echo -e "\e[92m       \`--.______        '\"\`.\t\e[33mpara o Workshop de Realidade Virtual Aumentada!"
echo -e "\e[92m          \__,__,\`---._   '\`; "
echo -e "\e[92m               ))\`-^--')\`,-' "
echo -e "\e[92m             ,',_____,'  | "
echo -e "\e[92m             \_          \`)." 
echo -e "\e[92m               \`.      _,'  \`" 
echo -e "\e[92m               /\`-._,-'      \\ \t\e[34mBugs do script: ruifilipe@ua.pt\e[0m"
echo ""
echo -e "\e[101m\e[97mATENÇÃO:\e[49m\e[91mSe já tem alguma pasta no Desktop\e[0m"
echo -e "\e[49m\e[91m\tchamada \e[44m\e[97mRVA\e[49m\e[91m será substituida, perdendo o conteudo! \e[0m"
echo ""
echo -e "\e[32mPrima <enter> para continuar...\e[0m"
read
clear

sudo apt-get install -y wget build-essential g++ libvtk5.8 libvtk5-dev libopencv-dev # freeglut3 freeglut3-dev binutils-gold
#sudo apt-get install -y eclipse eclipse-cdt

sudo apt-get install doxygen 
sudo apt-get install libsdl1.2-dev 
sudo apt-get install libgtk2.0-dev 
sudo apt-get install freeglut3-dev 
sudo apt-get install libboost-dev 
sudo apt-get install libxmu-dev 
sudo apt-get install libfreetype6-dev 
sudo apt-get install libgl1-mesa-dev 
sudo apt-get install libglu1-mesa-dev
sudo apt-get install libv4l-dev 
sudo apt-get install libgtk2.0-dev
sudo apt-get install libgtk2.0-0

#sudo apt-get install -y libmozjs

sudo apt-get install ibv4l-dev
sudo apt-get install libltdl-dev

echo -e "\e[32mPrima <enter> para continuar...\e[0m"
clear

echo -e "\e[33mSoftware instalado!\e[0m"
echo ""
echo -e "\e[32mPrima <enter> para continuar...\e[0m"
read
clear

cd ~/Desktop
rm -rf RVA
mkdir RVA
cd RVA

#rm ARToolKit-2.72.1.tgz
#wget http://downloads.sourceforge.net/project/artoolkit/artoolkit/2.72.1/ARToolKit-2.72.1.tgz
#wget http://www.kameda-lab.org/_local/imagelab.tsukuba.ac.jp/ubuntu1004+opencv21/ARToolKitk/ARToolKit-2.72.1.tgz
#wget http://www.kameda-lab.org/_local/imagelab.tsukuba.ac.jp/ubuntu1004+opencv21/ARToolKitk/artk-v4l2-2.72.1.20101003.patch

wget http://downloads.sourceforge.net/project/artoolkit/artoolkit/2.72.1/ARToolKit-2.72.1.tgz

tar zxfv ARToolKit-2.72.1.tgz
#tar -zxfv openvrml-0.16.2.tar.gz

sudo ln -s /usr/lib/libXmu.so.6 /usr/lib/libXmu.so
sudo ln -s /usr/lib/libXmuu.so.1 /usr/lib/libXmuu.so

#rm -rf ARToolKit
#tar xzvf ARToolKit-2.72.1.tgz

#patch -p0 -d . < artk-v4l2-2.72.1.20101003.patch

cd ARToolKit


#nautilus . &

sudo ln -s /usr/include/libv4l1-videodev.h /usr/include/linux/videodev.h

#sudo ln -s  /usr/include/linux/videodev2.h  /usr/include/linux/videodev.h

echo -e "\e[101m\e[97mATENÇÃO:\e[49m\e[91mSelecciona as seguintes opções no menu a seguir:\e[0m"
echo "1º select V4L2 3rd option"
echo "2º select y (X86)"
echo "3º select n"
echo "4º select n"
echo -e "\e[33mNão te esqueças!\e[0m"
echo ""
echo -e "\e[32mPrima <enter> para ler os valores...\e[0m"

./Configure

make