#!/bin/bash
clear

echo -e "\e[92m     .--,       .--,"
echo -e "\e[92m    ( (  \.---./  ) )\t\t\e[101mOlá $USER!\e[0m"
echo -e "\e[92m     '.__/o   o\__.'"
echo -e "\e[92m        {=  ^  =}"
echo -e "\e[92m         >  -  <      \e[33m\tVai proceder à instalação do software para o Micro-Rato!"
echo -e "\e[92m        /       \\    \e[33m\tSoftware disponivel em:"
echo -e "\e[92m       //       |\\   \e[33m\t\thttp://sweet.ua.pt/jla/"
echo -e "\e[92m      //|   .   | \\"
echo -e "\e[92m      \"'\\       /'\"_.-~^''-."
echo -e "\e[92m         \\  _  /--'         '"
echo -e "\e[92m       ___)( )(___    \e[34m\tBugs do script: ruifilipe@ua.pt\e[0m"
echo -e "\e[92m      (((__) (__)))"
echo ""
echo -e "\e[101m\e[97mATENÇÃO:\e[49m\e[91m\tSe já tem alguma pasta no Desktop\e[0m"
echo -e "\e[49m\e[91m\t\tchamada \e[44m\e[97mMicroRato\e[49m\e[91m será substituida, perdendo o conteudo! \e[0m"
echo ""
echo -e "\e[32mPrima <enter> para continuar...\e[0m"
read
clear

sudo apt-get install -y wget geany build-essential
echo -e "\e[32mPrima <enter> para continuar...\e[0m"
clear

MACHINE_TYPE=`uname -m`
if [ ${MACHINE_TYPE} == 'x86_64' ]; then
  echo -e "\e[32mA sua maquina é de 64 bits.\e[0m"
  rm /tmp/pic32-64-2013_10_29.tgz
  cd /tmp
  wget http://sweet.ua.pt/jla/Sw/pic32-64-2013_10_29.tgz
  sudo tar xzvf /tmp/pic32-64-2013_10_29.tgz -C /opt
  rm /tmp/pic32-64-2013_10_29.tgz
else
  echo -e "\e[32mA sua maquina é de 32 bits.\e[0m"
  rm /tmp/pic32-32-2013_10_29.tgz
  cd /tmp
  wget http://sweet.ua.pt/jla/Sw/pic32-32-2013_10_29.tgz
  sudo tar xzvf /tmp/pic32-32-2013_10_29.tgz -C /opt
  rm /tmp/pic32-32-2013_10_29.tgz
fi

sudo usermod -a -G dialout $USER

cd ~

if grep -Fxq "if [ -d /opt/pic32mx/bin ] ; then" .bashrc
then
	echo -e "\e[101mJá tem o .bashrc configurado!\e[0m"
else
	echo -e "\n\n##################################" >> .bashrc
	echo -e "#MicroRato Tools:" >> .bashrc
	echo -e "if [ -d /opt/pic32mx/bin ] ; then" >> .bashrc
	echo -e "\texport PATH=$PATH:/opt/pic32mx/bin" >> .bashrc
	echo -e "fi" >> .bashrc
	echo -e "##################################\n\n" >> .bashrc
fi

source ~/.bashrc

echo -e "\e[33mSoftware instalado!\e[0m"
echo -e "\e[33mCompilador: pcompile\e[0m"
echo -e "\e[33mEscrever na pic: ldpic32 -w file.hex\e[0m"
echo -e "\e[33mTerminal: pterm\e[0m"
echo ""
echo -e "\e[33mNo final reinicie a maquina, ou faça o load do ficheiro .bashrc com: source .bachrc\e[0m"
echo -e "\e[32mPrima <enter> para continuar...\e[0m"
read
clear

cd ~/Desktop
rm -rf MicroRato
mkdir MicroRato

cd MicroRato
wget http://sweet.ua.pt/jla/Docs/MR32-funcoes.pdf
wget http://sweet.ua.pt/jla/Sw/DETPIC32-system_calls.pdf
wget http://sweet.ua.pt/jla/Sw/servo_calib.tgz
wget http://sweet.ua.pt/jla/Sw/mr32-2013_11_07.tgz

mkdir mr32-2013_11_07
tar xzvf mr32-2013_11_07.tgz -C mr32-2013_11_07
rm mr32-2013_11_07.tgz

echo -e "\e[33mNo seu ambiente de trabalho contem a pasta MicroRato/mr32-2013_11_07 com as bibliotecas do MicroRato\e[0m"
echo -e "\e[33mVou criar agora mesmo um programa exemplo, chamado 'superRato.c'\e[0m"
echo -e "\e[32mPrima <enter> para continuar...\e[0m"
read

cd mr32-2013_11_07
touch superRato.c

echo -e "#include \"mr32.h\"" > superRato.c
echo -e "int main(void){" >> superRato.c
echo -e "\t// variáveis declaradas AQUI:" >> superRato.c
echo -e "\t//..." >> superRato.c
echo -e "\tinitPIC32();"  >> superRato.c
echo -e "\twhile(TRUE){"  >> superRato.c
echo -e "\t\t// programa inserido AQUI:" >> superRato.c
echo -e "\t\t//..."  >> superRato.c
echo -e "\t}"  >> superRato.c
echo -e "\treturn 0;"  >> superRato.c
echo -e "}"  >> superRato.c

echo -e "\e[33msuperRato.c criado :)\e[0m"
echo -e "\e[32mPrima <enter> para terminar.\e[0m"
echo -e "\e[33m\e[0m"
echo -e "\e[44mRelembro:\e[0m"
echo -e "\t\e[45mpcompile\e[0m para compilar"
echo -e "\t\t\e[43mExemplo:\e[0m pcompile superRato.c mr32.c"
echo -e "\t\e[45mldpic32\e[0m para transferir"
echo -e "\t\t\e[43mExemplo:\e[0m ldpic32 -w superRato.hex"
echo -e "\t\e[45mpterm\e[0m para abrir terminal para comunicar com o rato"
echo -e "\t\t\e[43mExemplo:\e[0m pterm"
echo -e "\e[33mdone.\e[0m"
read

nautilus . &
geany mr32.c mr32.h superRato.c &