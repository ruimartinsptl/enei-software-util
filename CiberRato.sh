#!/bin/bash
clear

echo -e "\e[92m     .--,       .--,"
echo -e "\e[92m    ( (  \.---./  ) )\t\t\e[101mOlá $USER!\e[0m"
echo -e "\e[92m     '.__/o   o\__.'"
echo -e "\e[92m        {=  ^  =}"
echo -e "\e[92m         >  -  <      \e[33m\tVai proceder à instalação do software para o Ciber-Rato!"
echo -e "\e[92m        /       \\    \e[33m\tSoftware disponivel em:"
echo -e "\e[92m       //       |\\   \e[33m\t\thttp://sourceforge.net/projects/cpss/"
echo -e "\e[92m      //|   .   | \\"
echo -e "\e[92m      \"'\\       /'\"_.-~^''-."
echo -e "\e[92m         \\  _  /--'         '"
echo -e "\e[92m       ___)( )(___    \e[34m\tBugs do script: ruifilipe@ua.pt\e[0m"
echo -e "\e[92m      (((__) (__)))"
echo ""
echo -e "\e[101m\e[97mATENÇÃO:\e[49m\e[91m\tSe já tem alguma pasta no Desktop\e[0m"
echo -e "\e[49m\e[91m\t\tchamada \e[44m\e[97mCiberRato\e[49m\e[91m será substituida, perdendo o conteudo! \e[0m"
echo ""
echo -e "\e[32mPrima <enter> para continuar...\e[0m"
read
clear

sudo apt-get update
sudo apt-get install -y wget
sudo apt-get install -y geany 
sudo apt-get install -y build-essential 
sudo apt-get install -y qt4-qmake
sudo apt-get install -y qt4-bin-dbg
sudo apt-get install -y qt4-default
echo -e "Software obrigatório instalado!"
echo -e "Indique agora se quer instalar opcionalmente o JAVAC!"
echo -e "\e[32mPrima <enter> para continuar...\e[0m"
read
clear

echo -e "Se ainda não tens um javac, instale o seguinte pacote openjdk-6-jdk:"
sudo apt-get install openjdk-6-jdk 
echo -e "\e[32mPrima <enter> para continuar...\e[0m"
read
clear

cd ~/Desktop
rm -rf CiberRato
mkdir CiberRato
cd CiberRato

wget http://downloads.sourceforge.net/project/cpss/cpss/2.2.0/cibertools-v2.2.tgz

nautilus . &

#mkdir cibertools-v2.2
#tar xzvf cibertools-v2.2.tgz -C cibertools-v2.2
tar xzvf cibertools-v2.2.tgz
rm cibertools-v2.2.tgz

rm Ciber2014-rules.pdf
wget https://bitbucket.org/ruimartinsptl/enei-software-util/downloads/Ciber2014-rules.pdf

echo -e "\e[33mNo seu ambiente de trabalho contem a pasta CiberRato/cibertools-v2.2 com as bibliotecas do MicroRato\e[0m"
echo -e "\e[33mVamos compilar um cliente JAVA e po-lo a funcionar...\e[0m"
echo -e "\e[32mPrima <enter> para continuar...\e[0m"
read

cd cibertools-v2.2
make -j8 # Compila o simulador e o visualizador

cd jClient
make # Compila o jClient.java

cd ..
./startSimViewer & # Inicia o simulador e o visualizador
sleep 5
cd jClient
./s5.sh	& # Arranca os 5 ratos

echo -e "Tudo Ok."
echo -e "\e[32mPrima <enter> para terminar...\e[0m"
read